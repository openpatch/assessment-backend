FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

ENV OPENPATCH_SERVICE_NAME=assessment
ENV OPENPATCH_AUTHENTIFICATION_SERVICE=https://api.openpatch.app/authentification
ENV OPENPATCH_ITEMBANK_SERVICE=https://api.openpatch.app/itembank
ENV OPENPATCH_FORMAT_SERVICE=http://openpatch-format_backend
ENV OPENPATCH_DB=sqlite+pysqlite:///database.db
ENV OPENPATCH_ORIGINS=*

COPY "." "/var/www/app"

RUN pip3 install -r /var/www/app/requirements.txt
