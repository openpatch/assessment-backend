from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.jwt import get_jwt_claims
from openpatch_assessment.api.v1 import api, errors
from openpatch_assessment.api.v1.schemas.node_result import NODE_RESULT_SCHEMA
from openpatch_assessment.models.node_result import NodeResult
from sqlalchemy.exc import StatementError

base_url = "/node-results"


@api.route(base_url + "/<node_result_id>", methods=["GET"])
def node_result(node_result_id):
    jwt_claims = get_jwt_claims()

    try:
        node_result = NodeResult.query.get(node_result_id)
    except StatementError:
        return errors.resource_not_found()

    if not node_result:
        return errors.resource_not_found()

    if not node_result.test_result.assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    node_result_dump = NODE_RESULT_SCHEMA.dump(node_result)
    node_result_dump["assessment"] = node_result.test_result.assessment.id

    return jsonify({"node_result": node_result_dump})
