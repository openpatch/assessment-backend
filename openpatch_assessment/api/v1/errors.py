from openpatch_core.errors import error_manager as em


def invalid_json(errors):
    return em.make_json_error(
        400, message="Invalid JSON in request body", details=errors, code=1
    )


def access_not_allowed():
    """
    @apiDefine errors_access_not_allowed
    @apiError (ErrorCode 1) {String} message AccessNotAllowed
    @apiError (ErrorCode 1) {Integer} status_code 403
    @apiError (ErrorCode 1) {Integer} code 1
    """
    return em.make_json_error(
        403, message="Your credentials do not allow access to this resource", code=1
    )


def resource_not_found():
    """
    @apiDefine errors_resource_not_found
    @apiError (ErrorCode 110) {String} message ResourceNotFound
    @apiError (ErrorCode 110) {Integer} status_code 404
    @apiError (ErrorCode 110) {Integer} code 110
    """
    return em.make_json_error(404, message="Resource not found", code=110)


def no_json():
    """
    @apiDefine errors_no_json
    @apiError (ErrorCode 201) {String} message NoJson
    @apiError (ErrorCode 201) {Integer} status_code 400
    @apiError (ErrorCode 201) {Integer} code 201
    """
    return em.make_json_error(400, message="JSON in request body is missing", code=201)


def invalid_session():
    return em.make_json_error(400, message="Invalid Session", code=2)


def assessment_not_found():
    return em.make_json_error(404, message="Assessment not found", code=3)


def assessment_expired():
    return em.make_json_error(400, message="Assessment expired", code=4)


def assessment_inactive():
    return em.make_json_error(400, message="Assessment inactive", code=5)


def format_service_down():
    return em.make_json_error(400, message="Format Service down", code=6)


def password_protected():
    return em.make_json_error(400, message="Assessment is password protected", code=7)
