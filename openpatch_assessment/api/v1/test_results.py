from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.jwt import get_jwt_claims
from openpatch_assessment.api.v1 import api, errors
from openpatch_assessment.api.v1.schemas.test_result import TEST_RESULT_SCHEMA
from openpatch_assessment.models.test_result import TestResult
from sqlalchemy.exc import StatementError

base_url = "/test-results"


@api.route(base_url + "/<test_result_id>", methods=["GET", "DELETE"])
def test_result(test_result_id):
    if request.method == "GET":
        return get_test_result(test_result_id)
    elif request.method == "DELETE":
        return delete_test_result(test_result_id)


def get_test_result(test_result_id):
    jwt_claims = get_jwt_claims()

    try:
        test_result = TestResult.query.get(test_result_id)
    except StatementError:
        return errors.resource_not_found()

    if not test_result:
        return errors.resource_not_found()

    if not test_result.assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    return jsonify({"test_result": TEST_RESULT_SCHEMA.dump(test_result)})


def delete_test_result(test_result_id):
    jwt_claims = get_jwt_claims()

    try:
        test_result = TestResult.query.get(test_result_id)
    except StatementError:
        return errors.resource_not_found()

    if not test_result:
        return errors.resource_not_found()

    if not test_result.assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    db.session.delete(test_result)
    db.session.commit()

    return jsonify({"status": "ok"})
