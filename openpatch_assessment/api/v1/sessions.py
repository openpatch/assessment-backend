from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_assessment.api.v1 import api, errors
from openpatch_assessment.api.v1.schemas.test_result import TEST_RESULT_SCHEMA
from openpatch_assessment.api.v1.schemas.item_result import ITEM_RESULTS_SCHEMA
from openpatch_assessment.api.v1.schemas.item_version_cache import (
    ITEM_VERSION_CACHES_SCHEMA,
)
from openpatch_assessment.models.test_result import TestResult
from openpatch_assessment.models.node_result import NodeResult
from openpatch_assessment.models.item_result import ItemResult
from openpatch_assessment.models.assessment import Assessment
from openpatch_assessment.models.item_version_cache import ItemVersionCache
from datetime import datetime
import requests
import os

base_url = "/sessions"

"""
prepare -> start?id=GUID -> /?id=GUID [POST] -> /?id=GUID [GET] 1-> /result
                                                                2-> /abort
"""


@api.route(base_url, methods=["POST", "GET"])
def root():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()

    if not test_result:
        return errors.invalid_session()

    if request.method == "POST":
        return post_solutions(test_result, session_hash)
    return get_next_items(test_result, session_hash)


def post_solutions(test_result, session_hash):
    """
    @api {post} /v1/sessions Post solutions
    @apiVersion 1.0.0
    @apiName PostSolution
    @apiGroup Sessions
    """
    solutions = request.json.get("solutions", [])

    current_node_result = test_result.node_results[-1]

    if current_node_result is None:
        return errors.invalid_session()

    # collection of all evaluation correct states
    checks = []
    passed = True

    node_flow = current_node_result.node.get("flow")
    encrypted = current_node_result.node.get("encrypted")
    if node_flow == "linear":
        try:
            solution = solutions[0]
        except IndexError:
            solution = None
            pass
        item_result = current_node_result.item_results[-1]

        evaluation = {"correct": True}
        if not encrypted:
            evaluation = evaluate_solution(item_result, solution)
        else:
            item_result.solution = None
            item_result.solution_encrypted = solution
            item_result.correct = True
            item_result.encrypted = True
            item_result.end = datetime.now()
            item_result.evaluation = evaluation

        # in dubio pro reo
        correct = evaluation.get("correct", True)
        checks.append(correct)
        passed = passed and correct

    elif node_flow == "jumpable":
        for i in range(len(current_node_result.item_results)):
            solution = {}
            try:
                solution = solutions[i]
            except IndexError:
                pass
            item_result = current_node_result.item_results[i]
            item_result.end = datetime.now()

            evaluation = {"correct": True}
            if not encrypted:
                evaluation = evaluate_solution(item_result, solution)
            else:
                item_result.solution = None
                item_result.solution_encrypted = solution
                item_result.correct = True
                item_result.encrypted = True
                item_result.end = datetime.now()
                item_result.evaluation = evaluation

            # in dubio pro reo
            correct = evaluation.get("correct", True)
            checks.append(correct)
            passed = passed and correct

    needs_to_be_correct = current_node_result.node.get("needs_to_be_correct", False)
    if not needs_to_be_correct:
        passed = True

    if passed:
        db.session.commit()
    else:
        db.session.rollback()

    return jsonify(
        {
            "passed": passed,
            "checks": checks,
            "session_hash": session_hash,
            "needs_to_be_correct": needs_to_be_correct,
        }
    )


def evaluate_solution(item_result, solution):
    format_url = os.getenv("OPENPATCH_FORMAT_SERVICE")
    if not format_url:
        return errors.format_service_down()

    evaluate_json = {"tasks": item_result.item.tasks, "solutions": solution}

    error_evaluation = False
    evaluation = {}
    correct = True
    try:
        r = requests.post("%s/v1/evaluate-batch" % format_url, json=evaluate_json)
        if r.status_code >= 500:
            error_evaluation = True
        elif r.status_code >= 400:
            error_evaluation = True

        evaluation = r.json()
        correct = evaluation.get("correct", True)

    except requests.ConnectionError:
        error_evaluation = True
    except ValueError:
        error_evaluation = True

    item_result.solution = solution
    item_result.correct = correct
    item_result.end = datetime.now()
    item_result.evaluation = evaluation

    return evaluation


def get_next_items(test_result, session_hash):
    assessment = test_result.assessment

    next = test_result.get_next()

    next_items = next.get("items", [])
    cleaned_items = []

    for item in next_items:
        tasks = item.tasks
        cleaned_tasks = []
        for task in tasks:
            cleaned_tasks.append(
                {
                    "data": task.get("data", {}),
                    "format_type": task.get("format_type"),
                    "format_version": task.get("format_version"),
                    "task": task.get("task"),
                    "text": task.get("text"),
                }
            )

        cleaned_items.append({"tasks": cleaned_tasks})

    return jsonify(
        {
            "language": assessment.language,
            "session_hash": session_hash,
            "items": cleaned_items,
            "encrypted": next.get("encrypted", False),
            "encryption_public_key": next.get("encryption_public_key", None),
            "recorded": next.get("recorded", False),
            "recording_id": next.get("recording_id", None),
        }
    )


@api.route(base_url + "/prepare", methods=["POST"])
def prepare():
    id = request.json.get("id")
    password = request.json.get("password")

    if not id:
        return errors.assessment_not_found()

    assessment = Assessment.query.get(id)

    if not assessment:
        return errors.assessment_not_found()

    if assessment.starts_on and assessment.starts_on > datetime.now():
        return errors.assessment_inactive()

    if assessment.ends_on and assessment.ends_on < datetime.now():
        return errors.assessment_expired()

    if (assessment.password_hash and not password) or (
        assessment.password_hash and not assessment.verify_password(password)
    ):
        return errors.password_protected()

    test_result = TestResult(
        assessment=assessment,
        test_id=assessment.test_id,
        test_version=assessment.test_version,
    )
    db.session.add(test_result)
    db.session.commit()

    return jsonify(
        {"session": test_result.session_hash, "language": assessment.language}
    )


@api.route(base_url + "/start", methods=["GET"])
def start():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()

    if not test_result:
        return errors.invalid_session()

    test_result.start = datetime.now()
    if len(test_result.node_results) > 0:
        test_result.node_results[0].start = datetime.now()
    db.session.commit()

    return jsonify(
        {
            "session_hash": test_result.session_hash,
            "language": test_result.assessment.language,
        }
    )


@api.route(base_url + "/farewell", methods=["GET"])
def farewell():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if not test_result:
        return errors.invalid_session()

    return jsonify(
        {
            "language": test_result.assessment.language,
            "farewell": test_result.assessment.farewell,
            "show_result": test_result.assessment.show_result,
        }
    )


@api.route(base_url + "/welcome", methods=["GET"])
def welcome():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if not test_result:
        return errors.invalid_session()

    return jsonify(
        {
            "welcome": test_result.assessment.greeting,
            "language": test_result.assessment.language,
        }
    )


@api.route(base_url + "/data-protection", methods=["GET"])
def dataProtection():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if not test_result:
        return errors.invalid_session()

    return jsonify(
        {
            "data_protection": test_result.assessment.data_protection,
            "record": test_result.assessment.record,
            "encrypt": test_result.assessment.encrypt,
            "language": test_result.assessment.language,
        }
    )


@api.route(base_url + "/result", methods=["GET"])
def result():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()

    if not test_result.assessment.show_result:
        return errors.access_not_allowed()

    item_results = (
        ItemResult.query.join(NodeResult)
        .join(TestResult)
        .filter_by(session_hash=session_hash)
        .all()
    )
    if not item_results:
        return errors.invalid_session()

    item_versions = (
        ItemVersionCache.query.join(
            ItemResult,
            db.and_(
                ItemVersionCache.item == ItemResult.item_id,
                ItemVersionCache.version == ItemResult.item_version,
            ),
        )
        .join(NodeResult)
        .join(TestResult)
        .filter_by(session_hash=session_hash)
        .all()
    )

    return jsonify(
        {
            "language": test_result.assessment.language,
            "item_results": ITEM_RESULTS_SCHEMA.dump(item_results),
            "item_versions": ITEM_VERSION_CACHES_SCHEMA.dump(item_versions),
            "score": test_result.score,
            "aborted": test_result.aborted,
        }
    )


@api.route(base_url + "/abort", methods=["POST"])
def abort():
    session_hash = request.args.get("session_hash")

    if not session_hash:
        return errors.invalid_session()

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if not test_result:
        return errors.invalid_session()

    reason = request.json.get("reason")
    test_result.end = datetime.now()
    test_result.aborted = True
    test_result.aborted_reason = reason
    db.session.commit()

    return jsonify({"msg": "ok"})
