from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims
from openpatch_assessment.api.v1 import api, errors
from openpatch_assessment.api.v1.schemas.assessment import (
    ASSESSMENT_SCHEMA,
    ASSESSMENTS_SCHEMA,
)
from openpatch_assessment.api.v1.schemas.test_result import TEST_RESULTS_SCHEMA
from openpatch_assessment.api.v1.schemas.item_version_cache import (
    ITEM_VERSION_CACHE_SCHEMA,
    ITEM_VERSION_CACHES_SCHEMA,
)
from openpatch_assessment.api.v1.schemas.test_version_cache import (
    TEST_VERSION_CACHE_SCHEMA,
)
from openpatch_assessment.models.assessment import Assessment
from openpatch_assessment.models.member import Member
from openpatch_assessment.models.test_result import TestResult
from openpatch_assessment.models.node_result import NodeResult
from openpatch_assessment.models.item_result import ItemResult
from openpatch_assessment.models.test_version_cache import TestVersionCache
from openpatch_assessment.models.item_version_cache import ItemVersionCache
from sqlalchemy.exc import DataError, StatementError
from sqlalchemy.sql import func

base_url = "/assessments"


@api.route(base_url + "/public", methods=["GET"])
def public_assessments():
    assessment_query, count, page = Assessment.elastic_query(
        request.args.get("query", "{}")
    )

    assessment_query = assessment_query.filter(db.or_(Assessment.public))

    count = assessment_query.count()
    assessment_query = page(query=assessment_query)
    assessments = assessment_query.all()

    return jsonify(
        {"assessments": ASSESSMENTS_SCHEMA.dump(assessments), "assessment_count": count}
    )


@api.route(base_url, methods=["GET", "POST"])
def assessments():
    if request.method == "POST":
        return post_assessments()
    return get_assessments()


def get_assessments():
    jwt_claims = get_jwt_claims(optional=True)

    assessment_query, count, page = Assessment.elastic_query(
        request.args.get("query", "{}")
    )

    if not jwt_claims:
        assessment_query = assessment_query.filter(db.or_(Assessment.public))

    elif jwt_claims.get("role") != "admin":
        assessment_query = assessment_query.filter(
            db.or_(
                Assessment.public,
                Assessment.member_id == jwt_claims.get("id"),
                Assessment.authors.any(member_id=jwt_claims.get("id")),
            )
        )

    count = assessment_query.count()
    assessment_query = page(query=assessment_query)
    assessments = assessment_query.all()

    return jsonify(
        {"assessments": ASSESSMENTS_SCHEMA.dump(assessments), "assessment_count": count}
    )


def post_assessments():
    jwt_claims = get_jwt_claims()

    assessment_json = request.get_json()
    if not assessment_json:
        return errors.no_json()

    member = Member.get_or_create(jwt_claims)
    if "member" in assessment_json:
        del assessment_json["member"]

    try:
        assessment = ASSESSMENT_SCHEMA.load(assessment_json, session=db.session)
        assessment.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    if assessment_json.get("password"):
        assessment.hash_password(assessment_json.get("password"))

    if (
        assessment_json.get("encryption_private_key") is not None
        and assessment_json.get("encryption_public_key") is not None
    ):
        assessment.encryption_public_key = assessment_json.get("encryption_public_key")
        assessment.encryption_private_key = assessment_json.get(
            "encryption_private_key"
        )
        assessment.encrypt = True

    test_version = TestVersionCache.get(assessment.test_id, assessment.test_version)

    db.session.add(assessment)
    db.session.commit()

    return jsonify({"assessment_id": assessment.id})


@api.route(base_url + "/<assessment_id>", methods=["GET", "PUT", "DELETE"])
def assessment(assessment_id):
    if request.method == "PUT":
        return put_assessment(assessment_id)
    elif request.method == "DELETE":
        return delete_assessment(assessment_id)
    return get_assessment(assessment_id)


def get_assessment(assessment_id):
    jwt_claims = get_jwt_claims()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()
    assessment_dump = ASSESSMENT_SCHEMA.dump(assessment)
    assessment_dump["encryption_private_key"] = assessment.encryption_private_key

    return jsonify({"assessment": assessment_dump})


def put_assessment(assessment_id):
    jwt_claims = get_jwt_claims()

    assessment_json = request.get_json()
    if not assessment_json:
        return errors.no_json()

    member = Member.get_or_create(jwt_claims)

    if "member" in assessment_json:
        del assessment_json["member"]

    try:
        assessment = Assessment.query.get(assessment_id)
        assessment.member = member
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    try:
        result = ASSESSMENT_SCHEMA.load(
            assessment_json, session=db.session, instance=assessment, partial=True
        )
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.commit()

    return jsonify({"assessment_id": assessment.id})


def delete_assessment(assessment_id):
    jwt_claims = get_jwt_claims()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    db.session.delete(assessment)

    return jsonify({})


@api.route(base_url + "/<assessment_id>/results", methods=["GET", "DELETE"])
def assessment_results(assessment_id):
    if request.method == "GET":
        return get_assessment_results(assessment_id)
    elif request.method == "DELETE":
        return delete_assessment_results(assessment_id)


def delete_assessment_results(assessment_id):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    test_results = TestResult.query.filter(
        TestResult.assessment_id == assessment_id
    ).all()

    for test_result in test_results:
        for node_result in test_result.node_results:
            for item_result in node_result.item_results:
                db.session.delete(item_result)
            db.session.delete(node_result)
        db.session.delete(test_result)
    db.session.commit()

    return jsonify({"status": "ok"})


def get_assessment_results(assessment_id):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    test_result_query, count, page = TestResult.elastic_query(
        request.args.get("query", "{}")
    )

    test_result_query = test_result_query.filter(
        TestResult.assessment_id == assessment_id
    )

    count = test_result_query.count()
    test_result_query = page(query=test_result_query)
    test_results = test_result_query.all()

    return jsonify(
        {
            "test_results": TEST_RESULTS_SCHEMA.dump(test_results),
            "test_results_count": count,
        }
    )


@api.route(base_url + "/<assessment_id>/statistic", methods=["GET"])
def get_assessment_statistic(assessment_id):
    jwt_claims = get_jwt_claims()
    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    test_result_query = TestResult.query.filter(
        TestResult.assessment_id == assessment_id
    )

    count = test_result_query.count()
    aborted = test_result_query.filter(TestResult.aborted != None).count()
    finished_query = test_result_query.filter(
        db.and_(TestResult.start != None, TestResult.end != None)
    )
    finished = finished_query.count()

    avg_subquery = test_result_query.with_entities(
        TestResult.score, TestResult.end, TestResult.start
    ).subquery()

    avg_score = db.session.query(func.avg(avg_subquery.c.score)).scalar()
    avg_time = 0

    test_results = finished_query.all()
    for test_result in test_results:
        avg_time += (test_result.end - test_result.start).total_seconds()

    if finished > 0:
        avg_time /= float(finished)

    if not avg_score:
        avg_score = 0

    if not avg_time:
        avg_time = 0

    return jsonify(
        {
            "assessment_statistic": {
                "aborted": aborted,
                "finished": finished,
                "count": count,
                "avg_score": float(avg_score),
                "avg_time": float(avg_time),
            }
        }
    )


@api.route(base_url + "/<assessment_id>/node-results", methods=["GET"])
def assessment_node_results(assessment_id):
    node_results_query, count, page = NodeResult.elastic_query(
        request.args.get("query", "{}")
    )

    node_results = (
        node_results_query.join(TestResult)
        .join(Assessment)
        .filter(Assessment.id == assessment_id)
    ).all()

    return jsonify(
        {
            "node_results": [
                {
                    "id": node_result.id,
                    "start": node_result.start,
                    "end": node_result.end,
                    "recorded": node_result.recorded,
                    "encrypted": node_result.encrypted,
                    "score": node_result.score,
                    "test_node_id": node_result.test_node_id,
                }
                for node_result in node_results
            ]
        }
    )


@api.route(base_url + "/<assessment_id>/item-results", methods=["GET"])
def assessment_item_results(assessment_id):
    item_results_query, count, page = ItemResult.elastic_query(
        request.args.get("query", "{}")
    )

    item_results = (
        item_results_query.join(NodeResult)
        .join(TestResult)
        .join(Assessment)
        .filter(
            db.and_(
                Assessment.id == assessment_id,
                ItemResult.end != None,
                ItemResult.start != None,
            )
        )
    ).all()

    return jsonify(
        {
            "item_results": [
                {
                    "id": item_result.id,
                    "correct": item_result.correct,
                    "solution": item_result.solution,
                    "solution_encrypted": item_result.solution_encrypted,
                    "encrypted": item_result.encrypted,
                    "evaluation": item_result.evaluation,
                    "error_evaluation": item_result.error_evaluation,
                    "recorded": item_result.recorded,
                    "start": item_result.start,
                    "end": item_result.end,
                    "item_id": item_result.item_id,
                    "item_version": item_result.item_version,
                    "test_item_id": item_result.test_item_id,
                    "test_node_id": item_result.node_result.test_node_id,
                }
                for item_result in item_results
            ]
        }
    )


@api.route(base_url + "/<assessment_id>/results/export", methods=["GET"])
def assessment_results_export(assessment_id):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    test_version = TestVersionCache.query.filter_by(
        test_id=assessment.test_id, version=assessment.test_version
    ).first()
    item_versions = []
    for node in test_version.nodes:
        test_items = node.get("items", [])
        for test_item in test_items:
            item_version = test_item.get("item_version")
            item_versions.append(
                ItemVersionCache.query.filter_by(
                    item=item_version.get("item_id"),
                    version=item_version.get("version"),
                ).first()
            )

    return jsonify(
        {
            "test_results": TEST_RESULTS_SCHEMA.dump(assessment.test_results),
            "test_version": TEST_VERSION_CACHE_SCHEMA.dump(test_version),
            "item_versions": ITEM_VERSION_CACHES_SCHEMA.dump(item_versions),
        }
    )


@api.route(base_url + "/<assessment_id>/test-version", methods=["GET"])
def get_assessment_test_version(assessment_id):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    test_version = (
        db.session.query(TestVersionCache)
        .join(
            Assessment,
            db.and_(
                TestVersionCache.test_id == Assessment.test_id,
                TestVersionCache.version == Assessment.test_version,
            ),
        )
        .filter(TestVersionCache.test_id == assessment.test_id)
        .first()
    )

    if not test_version:
        return errors.resource_not_found()

    test_version_json = TEST_VERSION_CACHE_SCHEMA.dump(test_version)
    if request.args.get("items"):
        item_versions = []
        for node in test_version.nodes:
            test_items = node.get("items", [])
            for test_item in test_items:
                item_version = test_item.get("item_version")
                item_versions.append(
                    ItemVersionCache.query.filter_by(
                        item=item_version.get("item_id"),
                        version=item_version.get("version"),
                    ).first()
                )
        test_version_json["items"] = ITEM_VERSION_CACHES_SCHEMA.dump(item_versions)

    return jsonify({"test_version": test_version_json})


@api.route(
    base_url + "/<assessment_id>/items/<item_id>/versions/<item_version>",
    methods=["GET"],
)
def get_assessment_item_version(assessment_id, item_id, item_version):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    item_version = ItemVersionCache.query.filter_by(
        item=item_id, version=item_version
    ).first()

    if not item_version:
        return errors.resource_not_found()

    return jsonify({"item_version": ITEM_VERSION_CACHE_SCHEMA.dump(item_version)})


@api.route(base_url + "/<assessment_id>/nodes/<node_id>/item-versions", methods=["GET"])
def get_assessment_node_item_versions(assessment_id, node_id):
    jwt_claims = get_jwt_claims()

    if not assessment_id:
        return errors.resource_not_found()

    try:
        assessment = Assessment.query.get(assessment_id)
    except StatementError:
        return errors.resource_not_found()

    if not assessment:
        return errors.resource_not_found()

    if not assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    test_version = TestVersionCache.query.filter_by(
        test_id=assessment.test_id, version=assessment.test_version
    ).first()

    if not test_version:
        return errors.resource_not_found()

    item_versions = []
    for node in test_version.nodes:
        if node.get("id") == node_id:
            test_items = node.get("items", [])
            for test_item in test_items:
                item_version = test_item.get("item_version")
                item_versions.append(
                    ItemVersionCache.query.filter_by(
                        item=item_version.get("item_id"),
                        version=item_version.get("version"),
                    ).first()
                )
            break

    return jsonify({"item_versions": ITEM_VERSION_CACHES_SCHEMA.dump(item_versions)})
