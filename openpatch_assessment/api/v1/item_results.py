from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.jwt import get_jwt_claims
from openpatch_assessment.api.v1 import api, errors
from openpatch_assessment.api.v1.schemas.item_result import ITEM_RESULT_SCHEMA
from openpatch_assessment.models.item_result import ItemResult
from sqlalchemy.exc import StatementError

base_url = "/item-results"


@api.route(base_url + "/<item_result_id>", methods=["GET"])
def item_result(item_result_id):
    jwt_claims = get_jwt_claims()

    try:
        item_result = ItemResult.query.get(item_result_id)
    except StatementError:
        return errors.resource_not_found()

    if not item_result:
        return errors.resource_not_found()

    if not item_result.node_result.test_result.assessment.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    item_result_dump = ITEM_RESULT_SCHEMA.dump(item_result)
    item_result_dump[
        "encryption_private_key"
    ] = item_result.node_result.test_result.assessment.encryption_private_key
    item_result_dump["assessment"] = item_result.node_result.test_result.assessment.id
    return jsonify({"item_result": item_result_dump})
