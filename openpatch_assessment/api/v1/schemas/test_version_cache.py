import re
from marshmallow import fields, EXCLUDE, Schema
from openpatch_core.schemas import ma
from openpatch_assessment.models.test_version_cache import TestVersionCache

uuid4hex = re.compile(
    "^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z", re.I
)


def validate_uuid4(value):
    return uuid4hex.match(value) is not None


class TestEdgeSchema(Schema):
    id = fields.String(validate=validate_uuid4)

    node_from = fields.String()
    node_to = fields.String()
    threshold = fields.Integer()


class ItemVersion(Schema):
    version = fields.Integer()
    item_id = fields.String()


class TestItem(Schema):
    id = fields.String(validate=validate_uuid4, required=True)
    item_version = fields.Nested(ItemVersion, required=True)


class TestNodeSchema(Schema):
    id = fields.String(validate=validate_uuid4)

    name = fields.Str()
    flow = fields.Str()

    needs_to_be_correct = fields.Boolean()
    randomized = fields.Boolean()

    time_limit = fields.Integer()
    item_limit = fields.Integer()
    score_limit = fields.Integer()
    precision_limit = fields.Integer()

    start = fields.Boolean()
    end = fields.Boolean()

    edges = fields.Nested(TestEdgeSchema, many=True)
    items = fields.Nested(TestItem, many=True)


class TestVersionCacheSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TestVersionCache
        unknown = EXCLUDE
        load_instance = True

    id = fields.UUID()

    nodes = fields.Nested(TestNodeSchema, many=True)


TEST_VERSION_CACHE_SCHEMA = TestVersionCacheSchema()
TEST_VERSION_CACHES_SCHEMA = TestVersionCacheSchema(many=True)
