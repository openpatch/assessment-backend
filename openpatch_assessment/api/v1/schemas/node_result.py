from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_assessment.models.node_result import NodeResult
from openpatch_assessment.api.v1.schemas.item_result import ItemResultSchema


class NodeResultSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = NodeResult
        load_instance = True
        include_relationships = True

    id = fields.UUID()

    item_results = fields.Nested(ItemResultSchema, many=True)


NODE_RESULT_SCHEMA = NodeResultSchema()
NODE_RESULTS_SCHEMA = NodeResultSchema(many=True)
