from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_assessment.models.item_result import ItemResult


class ItemResultSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemResult
        load_instance = True
        include_relationships = True

    id = fields.UUID()


ITEM_RESULT_SCHEMA = ItemResultSchema()
ITEM_RESULTS_SCHEMA = ItemResultSchema(many=True)
