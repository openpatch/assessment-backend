from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_assessment.models.test_result import TestResult
from openpatch_assessment.api.v1.schemas.node_result import NodeResultSchema


class TestResultSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TestResult
        load_instance = True
        include_relationships = True

    id = fields.UUID()

    node_results = fields.Nested(NodeResultSchema, many=True)


TEST_RESULT_SCHEMA = TestResultSchema()
TEST_RESULTS_SCHEMA = TestResultSchema(many=True)
