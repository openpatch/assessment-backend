from marshmallow import fields, EXCLUDE
from openpatch_core.schemas import ma
from openpatch_assessment.models.assessment import Assessment
from openpatch_assessment.api.v1.schemas.member import MemberSchema


class AssessmentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        unknown = EXCLUDE
        model = Assessment
        exclude = ("password_hash", "encryption_public_key", "encryption_private_key")
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    password = fields.Str()
    password_protected = fields.Function(lambda obj: obj.password_hash is not None)
    member = fields.Nested(
        MemberSchema, only=["id", "username", "full_name", "avatar_id"]
    )


ASSESSMENT_SCHEMA = AssessmentSchema()
ASSESSMENTS_SCHEMA = AssessmentSchema(many=True)
