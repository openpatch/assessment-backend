from marshmallow import fields, EXCLUDE, Schema
from openpatch_core.schemas import ma
from openpatch_assessment.models.item_version_cache import ItemVersionCache


class ItemTask(Schema):
    id = fields.String()

    task = fields.String()
    format_type = fields.String()
    format_version = fields.Integer()
    data = fields.Dict()
    evaluation = fields.Dict()
    text = fields.Dict()


class ItemVersionCacheSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemVersionCache
        unknown = EXCLUDE
        load_instance = True

    id = fields.UUID()

    item = fields.UUID()
    version = fields.Integer()

    tasks = fields.Nested(ItemTask, many=True)


ITEM_VERSION_CACHE_SCHEMA = ItemVersionCacheSchema()
ITEM_VERSION_CACHES_SCHEMA = ItemVersionCacheSchema(many=True)
