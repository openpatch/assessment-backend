from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_core.database import db
from openpatch_assessment.models.member import Member


class MemberSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Member
        sqla_session = db.session
        load_instance = True

    id = fields.UUID()


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
