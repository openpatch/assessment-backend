from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from openpatch_assessment.models.item_version_cache import ItemVersionCache
import uuid
from datetime import datetime


class ItemResult(Base):
    __tablename__ = gt("item_result")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    solution = db.Column(db.JSON)
    solution_encrypted = db.Column(db.Text)
    evaluation = db.Column(db.JSON)
    correct = db.Column(db.Boolean)
    error_evaluation = db.Column(db.Boolean)
    encrypted = db.Column(db.Boolean)
    recorded = db.Column(db.Boolean)

    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)

    item_id = db.Column(GUID())
    item_version = db.Column(db.Integer)
    test_item_id = db.Column(GUID())
    assessment_id = db.Column(GUID())

    node_result_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("node_result")))
    node_result = db.relationship("NodeResult", back_populates="item_results")

    position = db.Column(db.Integer)

    def __init__(self, *args, **kwargs):
        super(ItemResult, self).__init__(*args, **kwargs)
        self.start = datetime.now()

    @property
    def test_node_id(self):
        return self.node_result.test_node_id

    @property
    def test_result_id(self):
        return self.node_result.test_result.id

    @property
    def item(self):
        return ItemVersionCache.get(item_id=self.item_id, version=self.item_version)
