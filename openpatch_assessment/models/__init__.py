from openpatch_core.database import db
from openpatch_assessment.models import (
    assessment,
    assessment_author,
    item_result,
    item_version_cache,
    member,
    node_result,
    test_result,
    test_version_cache,
)
from sqlalchemy import orm

orm.configure_mappers()
