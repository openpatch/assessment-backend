from sqlalchemy.ext.orderinglist import ordering_list
from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from openpatch_assessment.models.item_result import ItemResult
from openpatch_assessment.models.test_version_cache import TestVersionCache
from openpatch_assessment.models.item_version_cache import ItemVersionCache
import uuid
import random
from datetime import datetime


class NodeResult(Base):
    __tablename__ = gt("node_result")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    test_result_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("test_result")))
    test_result = db.relationship("TestResult", back_populates="node_results")

    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)

    precision = db.Column(db.Integer)
    seed = db.Column(db.Integer)

    recorded = db.Column(db.Boolean)
    encrypted = db.Column(db.Boolean)

    test_id = db.Column(GUID())
    test_version = db.Column(db.Integer)
    test_node_id = db.Column(GUID())

    assessment_id = db.Column(GUID())

    score = db.Column(db.Integer)

    item_results = db.relationship(
        "ItemResult",
        back_populates="node_result",
        order_by="ItemResult.position",
        collection_class=ordering_list("position"),
    )

    def __init__(self, *args, **kwargs):
        super(NodeResult, self).__init__(*args, **kwargs)
        self.seed = random.randint(100, 10000)

    @property
    def node(self):
        test_version = TestVersionCache.get(
            test_id=self.test_id, version=self.test_version
        )
        return next(
            (
                node
                for node in iter(test_version.nodes)
                if node.get("id") == str(self.test_node_id)
            ),
            None,
        )

    @property
    def current_score(self):
        score = 0
        for item_result in self.item_results:
            if item_result.correct:
                score += 1
        return score

    def has_next_items(self):
        item_limit_reached = False
        node = self.node
        if node.get("item_limit", 0) > 0:
            item_limit_reached = len(self.item_results) >= node.get("item_limit", 0)

        time_limit_reached = False
        if node.get("time_limit", 0) > 0:
            time_limit_reached = self.start - datetime.datetime() > node.get(
                "time_limit"
            )

        score_limit_reached = False
        if node.get("score_limit", 0) > 0:
            score_limit_reached = self.current_score >= node.get("score_limit", 0)

        precision_limit_reached = False
        if node.get("precision_limit", 0) > 0:
            precision_limit_reached = self.precision <= node.get("precision_limit", 0)

        end_reached = False
        if (
            len(self.item_results) >= len(node.get("items", []))
            and len(self.item_results) > 0
            and self.item_results[-1].end is not None
        ):
            end_reached = True

        return not (
            item_limit_reached
            or time_limit_reached
            or score_limit_reached
            or precision_limit_reached
            or end_reached
        )

    def get_next_items(self):
        if not self.has_next_items():
            return None

        node = self.node

        items = node.get("items", [])
        if node.get("randomized", False):
            random.seed(self.seed)
            random.shuffle(items)

        node_type = node.get("flow")

        has_open_items = (
            len(self.item_results) > 0 and self.item_results[-1].end is None
        )

        next_items = []
        if node_type == "linear":
            if has_open_items:
                next_item = items[len(self.item_results) - 1]
            else:
                next_item = items[len(self.item_results)]
                self.item_results.append(
                    ItemResult(
                        assessment_id=self.assessment_id,
                        recorded=self.recorded,
                        encrypted=self.encrypted,
                        item_id=next_item["item_version"]["item_id"],
                        item_version=next_item["item_version"]["version"],
                        test_item_id=next_item["id"],
                    )
                )
                db.session.commit()
            next_items = [next_item]

        if node_type == "jumpable":
            if node.get("item_limit", 0) > 0:
                next_items = items[0 : node.get("item_limit")]
            else:
                next_items = items
            if not has_open_items:
                for next_item in next_items:
                    self.item_results.append(
                        ItemResult(
                            assessment_id=self.assessment_id,
                            recorded=self.recorded,
                            encrypted=self.encrypted,
                            item_id=next_item["item_version"]["item_id"],
                            item_version=next_item["item_version"]["version"],
                            test_item_id=next_item["id"],
                        )
                    )
                db.session.commit()

        if node_type == "adaptive":
            print("adaptive is not support at the moment")
            return None

        return [
            ItemVersionCache.get(
                item["item_version"]["item_id"], item["item_version"]["version"]
            )
            for item in next_items
        ]
