from flask import request
from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import os
import requests


class ItemVersionCache(Base):
    __tablename__ = gt("item_version_cache")

    item = db.Column(GUID(), primary_key=True)
    version = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(20))
    tasks = db.Column(db.JSON)

    def clear(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def clear_all(cls):
        db.session.delete(cls.query.all())
        db.session.commit()

    @classmethod
    def get(cls, item_id, version):
        item_version = cls.query.filter_by(item=item_id, version=version).first()
        return item_version
