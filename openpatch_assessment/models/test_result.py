from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from openpatch_assessment.models.node_result import NodeResult
from openpatch_assessment.models.test_version_cache import TestVersionCache
from hashids import Hashids
import uuid
from datetime import datetime


class TestResult(Base):
    __tablename__ = gt("test_result")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    assessment_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("assessment")))
    assessment = db.relationship("Assessment", back_populates="test_results")

    test_id = db.Column(GUID())
    test_version = db.Column(db.Integer)

    session_hash = db.Column(db.String(64), unique=True)

    aborted = db.Column(db.Boolean)
    aborted_reason = db.Column(db.Text)

    score = db.Column(db.Integer)

    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)

    node_results = db.relationship(
        "NodeResult", back_populates="test_result", order_by="NodeResult.start"
    )

    def __init__(self, *args, **kwargs):
        super(TestResult, self).__init__(*args, **kwargs)
        count = TestResult.query.count()
        self.session_hash = Hashids(min_length=7).encode(count)
        test_version = TestVersionCache.get(self.test_id, self.test_version)
        if not test_version:
            print("[error] missing test version cache")
        for node in test_version.nodes:
            if node.get("start", False):
                recorded = self.assessment.record and not node.get("encrypted", False)
                node_result = NodeResult(
                    recorded=recorded,
                    encrypted=node.get("encrypted", False),
                    test_node_id=node.get("id"),
                    test_id=self.test_id,
                    test_version=self.test_version,
                    assessment_id=self.assessment_id,
                )
                self.node_results.append(node_result)
                break
        db.session.commit()

    def abort(self, reason):
        self.aborted = True
        self.aborted_reason = reason
        self.end = datetime.now()
        self.calc_score()

    def submit_solutions(self, items_solutions):
        return node_results[-1].submit_solutions(items_solutions)

    def has_next_node(self):
        current_node_result = self.node_results[-1]

        if current_node_result.has_next_items():
            return True

        return len(current_node_result.node.get("edges", [])) > 0

    def calc_score(self):
        self.score = 0
        for node_result in self.node_results:
            node_result.score = node_result.current_score
            self.score += node_result.score
        return self.score

    def get_next_node_result(self):
        if not self.has_next_node():
            self.end = datetime.now()
            try:
                # set endtime when node is finished
                current_node_result = self.node_results[-1]
                current_node_result.end = datetime.now()
            except:
                pass
            self.calc_score()
            return None

        current_node_result = self.node_results[-1]

        if current_node_result.has_next_items():
            return current_node_result

        current_node_result.end = datetime.now()

        edges = current_node_result.node.get("edges", [])
        score = current_node_result.current_score

        next_node_id = None
        threshold = 0
        for edge in edges:
            if next_node_id is None and edge.get("threshold", 0) == 0:
                next_node_id = edge.get("node_to")
            elif (
                score >= edge.get("threshold", 0)
                and edge.get("threshold", 0) > threshold
            ):
                next_node_id = edge.get("node_to")

        # no id found
        if not next_node_id:
            return None

        # find the node
        test_version = TestVersionCache.get(self.test_id, self.test_version)
        for node in test_version.nodes:
            if node.get("id", None) == next_node_id:
                recorded = self.assessment.record and not node.get("encrypted", False)
                node_result = NodeResult(
                    assessment_id=self.assessment_id,
                    recorded=recorded,
                    encrypted=node.get("encrypted", False),
                    test_node_id=next_node_id,
                    test_id=self.test_id,
                    test_version=self.test_version,
                    start=datetime.now(),
                )
                self.node_results.append(node_result)
                return node_result

        # no node found
        return None

    def get_next(self):
        node_result = self.get_next_node_result()
        response = {"items": []}
        if node_result is None:
            db.session.commit()
            return response

        response["items"] = node_result.get_next_items()

        if node_result.recorded:
            if len(response["items"]) == 1:
                response["recording_id"] = node_result.item_results[-1].id
            elif len(response["items"]) > 1:
                response["recording_id"] = node_result.id

        if node_result.encrypted:
            response["encrypted"] = node_result.encrypted
            response["encryption_public_key"] = self.assessment.encryption_public_key

        response["recorded"] = node_result.recorded

        return response
