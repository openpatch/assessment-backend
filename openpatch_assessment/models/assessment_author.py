from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class AssessmentAuthor(Base):
    __tablename__ = gt("assessment_author")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    assessment_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("assessment"))))

    assessment = db.relationship("Assessment", back_populates="authors")
    member = db.relationship("Member", back_populates="assessments_author")
