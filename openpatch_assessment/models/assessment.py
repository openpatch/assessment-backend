from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from passlib.apps import custom_app_context as pwd_context
import uuid


class Assessment(Base):
    __tablename__ = gt("assessment")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    name = db.Column(db.String(256))
    starts_on = db.Column(db.DateTime)
    ends_on = db.Column(db.DateTime)
    public = db.Column(db.Boolean)
    language = db.Column(db.String(5))

    public_description = db.Column(db.JSON)
    greeting = db.Column(db.JSON)
    farewell = db.Column(db.JSON)
    data_protection = db.Column(db.JSON)

    show_progress = db.Column(db.Boolean)
    show_result = db.Column(db.Boolean)

    test_id = db.Column(GUID())
    test_version = db.Column(db.Integer)
    test_results = db.relationship("TestResult", back_populates="assessment")

    password_hash = db.Column(db.String(512))

    record = db.Column(db.Boolean)

    encrypt = db.Column(db.Boolean)
    encryption_private_key = db.Column(db.Text)
    encryption_public_key = db.Column(db.Text)

    member = db.relationship("Member", back_populates="assessments")
    authors = db.relationship("AssessmentAuthor", back_populates="assessment")

    def hash_password(self, password):
        self.password_hash = pwd_context.hash(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def permitted_write(self, jwt_claims):
        return (
            jwt_claims["id"] == str(self.member_id)
            or jwt_claims["id"] in [str(author.member.id) for author in self.authors]
            or jwt_claims["role"] == "admin"
        )

    def permitted_read(self, jwt_claims):
        return (
            self.public
            or jwt_claims["id"] == str(self.member_id)
            or jwt_claims["id"] in [str(author.member.id) for author in self.authors]
            or jwt_claims["role"] == "admin"
        )
