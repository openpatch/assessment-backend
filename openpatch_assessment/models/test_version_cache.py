from flask import request
import os
from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from openpatch_assessment.models.item_version_cache import ItemVersionCache
import uuid
import requests


class TestVersionCache(Base):
    __tablename__ = gt("test_version_cache")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    test_id = db.Column(GUID())
    version = db.Column(db.Integer)
    status = db.Column(db.String(20))
    nodes = db.Column(db.JSON)

    def clear(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def clear_all(cls):
        db.session.delete(cls.query.all())
        db.session.commit()

    @classmethod
    def get(cls, test_id, version):
        test_version = cls.query.filter_by(test_id=test_id, version=version).first()

        if (
            not test_version
            or test_version.status == "draft"
            or test_version.status is None
        ):
            headers = {"Authorization": request.headers.get("Authorization", None)}
            base_url = os.getenv("OPENPATCH_ITEMBANK_SERVICE")
            url = f"{base_url}/v1/tests/{test_id}/versions/{version}"
            r = requests.get(url, headers=headers)
            if r.status_code != 200:
                print("[error] fetching test for caching")
                return None
            data = r.json()
            test_version_json = data.get("test_version")
            if test_version and (
                test_version.status == "draft" or test_version.status is None
            ):
                test_version.test_id = test_id
                test_version.version = version
                test_version.status = test_version_json.get("status", "draft")
                test_version.nodes = test_version_json.get("nodes", [])
            else:
                test_version = TestVersionCache(
                    test_id=test_id,
                    version=version,
                    status=test_version_json.get("status", "draft"),
                    nodes=test_version_json.get("nodes", []),
                )
                db.session.add(test_version)

            headers = {"Authorization": request.headers.get("Authorization", None)}
            base_url = os.getenv("OPENPATCH_ITEMBANK_SERVICE")
            url = f"{base_url}/v1/tests/{test_id}/versions/{version}/item-versions"
            r = requests.get(url, headers=headers)
            if r.status_code != 200:
                print("[error] fetching items for caching")
                return None
            data = r.json()
            item_versions_json = data.get("item_versions", [])

            for iv in item_versions_json:
                item_version = ItemVersionCache.query.filter_by(
                    item=iv["item"], version=iv["version"]
                ).first()

                if not item_version:
                    item_version = ItemVersionCache(
                        item=iv["item"],
                        version=iv["version"],
                        tasks=iv["tasks"],
                        status=iv["status"],
                    )
                    db.session.add(item_version)
                elif item_version.status == "draft" or item_version.status is None:
                    item_version.item = iv["item"]
                    item_version.version = iv["version"]
                    tasks = iv["tasks"]
                    status = iv["status"]
            db.session.commit()

        return test_version
