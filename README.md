# Assessment Service

API Documentation: https://openpatch.gitlab.io/assessment-backend

## Environment Variables

| Environment Variable | Description | Default | Values |
| -------------------- | ----------- | ------- | ------ |
| OPENPATCH_MOCK | Fill the database with mock data | not set | "true" |
| OPENPATCH_MODE | Configures the configuration | "development" | "development", "production", "testing" |
| OPENPATCH_DB | URI for connecting to a database | sqlite:///database.db | \<protocol\>://\<path\> |
| OPENPATCH_AUTHENTIFICATION_SERVICE | url to authentification service | https://api.openpatch.app/authentification | a url |
| OPENPATCH_ITEMBANK_SERVICE | url to itembank service | https://api.openpatch.app/itembank | a url |
| OPENPATCH_FORMAT_SERVICE | url to format service | http://openpatch-format_backend | a url |
| OPENPATCH_ORIGINS | Allowed CORS origins | "*" | valid cors origin |
| SENTRY_DSN | Error tracking with Sentry | not set | a valid dsn |

## Start

```
docker-compose up
```

## Mock Data

```
docker-compose exec backend flask mock
```

## Testing

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend flask test
```

## Coverage

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend python3 coverage_report.py
```

## ER-Diagram

![ER-Diagram](.gitlab/er_diagram.png)
