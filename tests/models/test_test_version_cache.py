from tests.base_test import BaseTest
from openpatch_assessment.models.test_version_cache import TestVersionCache
from tests import mock


class TestTestVersionCache(BaseTest):
    def test_get(self):
        test_version = mock.test_versions[0]

        test_version_cache = TestVersionCache.query.filter_by(
            test_id=test_version["test_id"], version=test_version["version"]
        ).first()
        self.assertIsNotNone(test_version_cache)
        self.assertEqual(test_version_cache.status, "latest")

        test_version_cache = TestVersionCache.get(
            test_version["test_id"], test_version["version"]
        )
        self.assertIsNotNone(test_version_cache)
