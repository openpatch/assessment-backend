from tests.base_test import BaseTest
from openpatch_assessment.models.item_version_cache import ItemVersionCache
from tests import mock


class TestItemVersionCache(BaseTest):
    def test_get(self):
        item_version = mock.item_versions[0]

        item_version_cache = ItemVersionCache.query.filter_by(
            item=item_version["item"], version=item_version["version"]
        ).first()
        self.assertIsNotNone(item_version_cache)
        self.assertEqual(item_version_cache.status, "latest")

        item_version_cache = ItemVersionCache.get(
            item_version["item"], item_version["version"]
        )
        self.assertIsNotNone(item_version_cache)

