import json
import os
import httpretty
from tests.base_test import BaseTest
from openpatch_assessment.models.test_result import TestResult
from openpatch_assessment.models.node_result import NodeResult
from openpatch_assessment.models.item_result import ItemResult


class TestSessions(BaseTest):
    def test_linear_one_node(self):
        # Prepare
        response = self.client.post(
            "v1/sessions/prepare", json={"id": "b0424fb2-d584-46b6-bd7d-a48fd768503e"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("session", response.json)
        session = response.json["session"]
        test_result = TestResult.query.filter_by(session_hash=session).first()
        self.assertIn("language", response.json)
        self.assertEqual(response.json["language"], "de")
        self.assertIsNotNone(test_result)
        self.assertIsNone(test_result.start)
        self.assertIsNotNone(test_result.node_results)
        self.assertEqual(len(test_result.node_results), 1)
        self.assertIsNone(test_result.node_results[0].start)

        # Start
        response = self.client.get(f"v1/sessions/start?session_hash={session}")
        self.assertEqual(response.status_code, 200)
        test_result = TestResult.query.filter_by(session_hash=session).first()
        self.assertIsNotNone(test_result.start)
        self.assertIsNotNone(test_result.node_results[0].start)

        # Get Item
        def assert_get_response(
            name,
            before_item_results=-1,
            after_item_results=-1,
            before_node_results=-1,
            after_node_results=-1,
        ):
            test_result = TestResult.query.filter_by(session_hash=session).first()
            node_result = test_result.node_results[before_node_results - 1]
            self.assertEqual(len(node_result.item_results), before_item_results)

            response = self.client.get(f"v1/sessions?session_hash={session}")
            self.assertEqual(response.status_code, 200)

            test_result = TestResult.query.filter_by(session_hash=session).first()
            node_result = test_result.node_results[after_node_results - 1]
            self.assertEqual(len(node_result.item_results), after_item_results)
            item_result = node_result.item_results[after_item_results - 1]
            self.assertIsNotNone(item_result.start)
            self.assertIsNone(item_result.end)

            self.assertIn("language", response.json)
            self.assertEqual(response.json["language"], "de")
            self.assertIn("items", response.json)
            self.assertIn("encrypted", response.json)
            self.assertIn("encryption_public_key", response.json)
            self.assertIn("recorded", response.json)
            self.assertIn("recording_id", response.json)

            items = response.json["items"]
            self.assertEqual(len(items), 1)
            item = items[0]
            self.assertIn("tasks", item)

            tasks = item["tasks"]
            self.assertEqual(len(tasks), 1)

            task = tasks[0]
            self.assertIn("format_type", task)
            self.assertIn("format_version", task)
            self.assertIn("data", task)
            self.assertNotIn("evaluation", task)
            self.assertIn("task", task)
            self.assertIn("text", task)
            self.assertEqual(task["task"], name)

        assert_get_response(
            "Number One",
            before_item_results=0,
            after_item_results=1,
            before_node_results=1,
            after_node_results=1,
        )
        # call next items once again without posting. Should return the same items
        assert_get_response(
            "Number One",
            before_item_results=1,
            after_item_results=1,
            before_node_results=1,
            after_node_results=1,
        )

        # post item

        def assert_post_response(
            solutions, correct=True, item_results=0, node_results=0
        ):
            httpretty.register_uri(
                httpretty.POST,
                f"{os.getenv('OPENPATCH_FORMAT_SERVICE')}/v1/evaluate-batch",
                body=json.dumps({"correct": correct}),
            )
            test_result = TestResult.query.filter_by(session_hash=session).first()
            node_result = test_result.node_results[node_results - 1]
            self.assertEqual(len(node_result.item_results), item_results)
            item_result = node_result.item_results[item_results - 1]
            self.assertIsNotNone(item_result.start)
            self.assertIsNone(item_result.end)

            response = self.client.post(
                f"v1/sessions?session_hash={session}", json={"solutions": solutions}
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn("passed", response.json)
            self.assertIn("checks", response.json)
            self.assertIn("session_hash", response.json)
            self.assertIn("needs_to_be_correct", response.json)
            self.assertTrue(response.json["passed"])
            self.assertEqual(response.json["session_hash"], session)
            self.assertFalse(response.json["needs_to_be_correct"])

            test_result = TestResult.query.filter_by(session_hash=session).first()
            node_result = test_result.node_results[node_results - 1]
            self.assertEqual(len(node_result.item_results), item_results)
            item_result = node_result.item_results[item_results - 1]

        solutions = [{"choice": {"0": True}}]
        assert_post_response(solutions, correct=True, item_results=1, node_results=1)

        # get next items
        assert_get_response(
            "Number Two",
            before_item_results=1,
            after_item_results=2,
            before_node_results=1,
            after_node_results=1,
        )
        assert_post_response(solutions, correct=False, item_results=2, node_results=1)

        # get next items
        assert_get_response(
            "Number Three",
            before_item_results=2,
            after_item_results=3,
            before_node_results=1,
            after_node_results=1,
        )
        assert_post_response(solutions, correct=True, item_results=3, node_results=1)

        # no items left finished
        response = self.client.get(f"v1/sessions?session_hash={session}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json["items"]), 0)
        self.assertIn("language", response.json)
        self.assertEqual(response.json["language"], "de")

        # get farewell
        response = self.client.get(f"v1/sessions/farewell?session_hash={session}")
        self.assertEqual(response.status_code, 200)
        self.assertIn("farewell", response.json)
        self.assertIn("show_result", response.json)
        self.assertTrue(response.json["show_result"])
        self.assertIn("language", response.json)
        self.assertEqual(response.json["language"], "de")

        # access result
        response = self.client.get(f"v1/sessions/result?session_hash={session}")
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_results", response.json)
        self.assertIn("item_versions", response.json)
        self.assertIn("score", response.json)
        self.assertIn("aborted", response.json)
        self.assertIn("language", response.json)
        self.assertEqual(response.json["language"], "de")

        self.assertEqual(len(response.json["item_results"]), 3)
        self.assertEqual(len(response.json["item_versions"]), 3)
        self.assertEqual(response.json["score"], 2)
