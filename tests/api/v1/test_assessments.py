from tests.base_test import BaseTest
from openpatch_assessment.models.assessment import Assessment


class TestAssessments(BaseTest):
    def test_public(self):
        response = self.client.get("/v1/assessments/public")

        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            len(response.json["assessments"]),
            Assessment.query.filter_by(public=True).count(),
        )
        assessment = response.json["assessments"][0]
        self.assertIn("member", assessment)
        member = assessment["member"]

        self.assertIn("username", member)
        self.assertIn("id", member)
        self.assertIn("full_name", member)
        self.assertIn("avatar_id", member)
        self.assertNotIn("assessments", member)
        self.assertNotIn("assessments_author", member)
