from openpatch_core.database import db
from openpatch_assessment.api.v1.schemas.item_version_cache import (
    ITEM_VERSION_CACHES_SCHEMA,
)
from openpatch_assessment.api.v1.schemas.test_version_cache import (
    TEST_VERSION_CACHES_SCHEMA,
)
from openpatch_assessment.api.v1.schemas.member import MEMBERS_SCHEMA
from openpatch_assessment.api.v1.schemas.assessment import ASSESSMENTS_SCHEMA

import datetime
import uuid

members = [
    {"id": "250512d6-16e8-4161-8ac2-43501a7efe28"},
    {"id": "67a3eb02-a099-4930-bb9e-d6a91cb46a9c"},
    {"id": "749e10b4-9545-40ad-9f8a-8e41f08c817b"},
]

item_versions = [
    {
        "version": 1,
        "version_message": "hi",
        "item": str(uuid.uuid4()),
        "status": "latest",
        "tasks": [
            {
                "task": "Number One",
                "format_type": "choice",
                "format_version": 1,
                "data": {"choices": ["1", "2", "3"]},
                "evaluation": {"choices": {"1": True}},
            }
        ],
    },
    {
        "version": 1,
        "item": str(uuid.uuid4()),
        "status": "pilot",
        "tasks": [
            {
                "task": "Number Two",
                "format_type": "choice",
                "format_version": 1,
                "data": {"choices": ["1", "2", "3"]},
                "evaluation": {"choices": {"2": True}},
            }
        ],
    },
    {
        "version": 1,
        "item": str(uuid.uuid4()),
        "status": "pilot",
        "tasks": [
            {
                "task": "Number Three",
                "format_type": "choice",
                "format_version": 1,
                "data": {"choices": ["1", "2", "3"]},
                "evaluation": {"choices": {"3": True}},
            }
        ],
    },
]

test_versions = [
    {
        "test_id": str(uuid.uuid4()),
        "version": 1,
        "status": "latest",
        "nodes": [
            {
                "id": str(uuid.uuid4()),
                "flow": "linear",
                "needs_to_be_correct": False,
                "start": True,
                "randomized": False,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[0]["version"],
                            "item_id": item_versions[0]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[1]["version"],
                            "item_id": item_versions[1]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[2]["version"],
                            "item_id": item_versions[2]["item"],
                        },
                    },
                ],
            }
        ],
    },
    {
        "test_id": str(uuid.uuid4()),
        "version": 1,
        "nodes": [
            {
                "id": str(uuid.uuid4()),
                "flow": "linear",
                "needs_to_be_correct": False,
                "start": True,
                "randomized": True,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[0]["version"],
                            "item_id": item_versions[0]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[1]["version"],
                            "item_id": item_versions[1]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[2]["version"],
                            "item_id": item_versions[2]["item"],
                        },
                    },
                ],
            }
        ],
    },
    {
        "test_id": str(uuid.uuid4()),
        "version": 1,
        "nodes": [
            {
                "id": str(uuid.uuid4()),
                "flow": "jumpable",
                "needs_to_be_correct": False,
                "start": True,
                "randomized": True,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[0]["version"],
                            "item_id": item_versions[0]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[1]["version"],
                            "item_id": item_versions[1]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[2]["version"],
                            "item_id": item_versions[2]["item"],
                        },
                    },
                ],
            }
        ],
    },
    {
        "test_id": str(uuid.uuid4()),
        "version": 1,
        "nodes": [
            {
                "id": str(uuid.uuid4()),
                "flow": "linear",
                "needs_to_be_correct": True,
                "start": True,
                "randomized": False,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[0]["version"],
                            "item_id": item_versions[0]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[1]["version"],
                            "item_id": item_versions[1]["item"],
                        },
                    },
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[2]["version"],
                            "item_id": item_versions[2]["item"],
                        },
                    },
                ],
            }
        ],
    },
    {
        "test_id": str(uuid.uuid4()),
        "version": 1,
        "nodes": [
            {
                "id": "caefa228-b63e-4c7d-a0eb-ce359709176f",
                "flow": "linear",
                "needs_to_be_correct": False,
                "start": True,
                "randomized": False,
                "edges": [
                    {
                        "node_from": "caefa228-b63e-4c7d-a0eb-ce359709176f",
                        "node_to": "b4257c29-d8f5-4eb5-8ced-baee80baeb23",
                        "threshold": 0,
                    },
                    {
                        "node_from": "caefa228-b63e-4c7d-a0eb-ce359709176f",
                        "node_to": "2646c8b2-95d5-418d-8972-e2a5b30c5ab6",
                        "threshold": 1,
                    },
                ],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[0]["version"],
                            "item_id": item_versions[0]["item"],
                        },
                    }
                ],
            },
            {
                "id": "b4257c29-d8f5-4eb5-8ced-baee80baeb23",
                "flow": "linear",
                "needs_to_be_correct": True,
                "randomized": False,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[1]["version"],
                            "item_id": item_versions[1]["item"],
                        },
                    }
                ],
            },
            {
                "id": "2646c8b2-95d5-418d-8972-e2a5b30c5ab6",
                "flow": "linear",
                "needs_to_be_correct": True,
                "randomized": False,
                "edges": [],
                "items": [
                    {
                        "id": str(uuid.uuid4()),
                        "item_version": {
                            "version": item_versions[2]["version"],
                            "item_id": item_versions[2]["item"],
                        },
                    }
                ],
            },
        ],
    },
]

assessments = [
    {
        "id": "b0424fb2-d584-46b6-bd7d-a48fd768503e",
        "member": members[0],
        "language": "de",
        "name": "Current",
        "greeting": {
            "blocks": [
                {
                    "key": "7hms3",
                    "text": "Das ist ein Test. Bold",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [{"offset": 18, "length": 4, "style": "BOLD"}],
                    "entityRanges": [],
                    "data": {},
                }
            ],
            "entityMap": {},
        },
        "farewell": {},
        "data_protection": {},
        "show_progress": False,
        "show_result": True,
        "test_id": test_versions[0]["test_id"],
        "test_version": test_versions[0]["version"],
    },
    {
        "id": "0618c2ec-f12a-4da7-bb2c-0acacfe08a8e",
        "member": members[0],
        "name": "Random",
        "greeting": {
            "blocks": [
                {
                    "key": "7hms3",
                    "text": "Das ist ein Test. Bold.",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [{"offset": 18, "length": 4, "style": "BOLD"}],
                    "entityRanges": [],
                    "data": {},
                }
            ],
            "entityMap": {},
        },
        "farewell": {},
        "data_protection": {},
        "show_progress": False,
        "show_result": False,
        "test_id": test_versions[1]["test_id"],
        "test_version": test_versions[1]["version"],
    },
    {
        "id": "a7e174a6-98c9-4e80-ab2f-a74c8a73cb30",
        "public": True,
        "name": "Jumpy",
        "member": members[0],
        "greeting": {
            "blocks": [
                {
                    "key": "7hms3",
                    "text": "Das ist ein Test. Bold. Jumpy",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [{"offset": 18, "length": 4, "style": "BOLD"}],
                    "entityRanges": [],
                    "data": {},
                }
            ],
            "entityMap": {},
        },
        "farewell": {},
        "data_protection": {},
        "show_progress": False,
        "show_result": False,
        "test_id": test_versions[2]["test_id"],
        "test_version": test_versions[2]["version"],
    },
    {
        "id": "301a9e44-8607-401f-85bf-ff5a45804465",
        "name": "Only Correct Will Pass",
        "member": members[0],
        "greeting": {
            "blocks": [
                {
                    "key": "7hms3",
                    "text": "Das ist ein Test. Bold. Pass",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [{"offset": 18, "length": 4, "style": "BOLD"}],
                    "entityRanges": [],
                    "data": {},
                }
            ],
            "entityMap": {},
        },
        "farewell": {},
        "data_protection": {},
        "show_progress": False,
        "show_result": False,
        "test_id": test_versions[3]["test_id"],
        "test_version": test_versions[3]["version"],
    },
    {
        "id": "b07983f0-53f1-438e-920f-ae7d8a4d98b1",
        "name": "Decide for me",
        "member": members[0],
        "greeting": {
            "blocks": [
                {
                    "key": "7hms3",
                    "text": "Das ist ein Test. Bold. Decide",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [{"offset": 18, "length": 4, "style": "BOLD"}],
                    "entityRanges": [],
                    "data": {},
                }
            ],
            "entityMap": {},
        },
        "show_progress": False,
        "show_result": False,
        "test_id": test_versions[4]["test_id"],
        "test_version": test_versions[4]["version"],
    },
]


def mock():
    result = MEMBERS_SCHEMA.load(members, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = ITEM_VERSION_CACHES_SCHEMA.load(item_versions, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = TEST_VERSION_CACHES_SCHEMA.load(test_versions, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    result = ASSESSMENTS_SCHEMA.load(assessments, session=db.session)
    db.session.add_all(result)
    db.session.commit()
