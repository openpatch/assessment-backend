## [1.3.3](https://gitlab.com/openpatch/assessment-backend/compare/v1.3.2...v1.3.3) (2021-01-24)


### Bug Fixes

* support sqlite3 ([d482be5](https://gitlab.com/openpatch/assessment-backend/commit/d482be5b6923c8e5a52c1db51dada5c5d8a95b8e))

## [1.3.2](https://gitlab.com/openpatch/assessment-backend/compare/v1.3.1...v1.3.2) (2020-06-06)


### Bug Fixes

* mocking of data ([a654364](https://gitlab.com/openpatch/assessment-backend/commit/a6543645103fe7833026e29b348596408eeda4f7))
